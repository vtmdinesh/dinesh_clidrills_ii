echo "Downloading the contents of Harry Potter and the Goblet of fire:" 

wget https://raw.githubusercontent.com/bobdeng/owlreader/master/ERead/assets/books/Harry%20Potter%20and%20the%20Goblet%20of%20Fire.txt 

echo "Printing the first three lines in the book:"

head -3 "Harry Potter and the Goblet of Fire.txt"

echo "Printing the last ten lines in the book:"

tail -10 "Harry Potter and the Goblet of Fire.txt" 

echo "Counting number of Occurrence of words:"
echo "Harry:"
grep -io Harry "Harry Potter and the Goblet of Fire.txt" | wc -w
echo "Ron:"
grep -io Ron "Harry Potter and the Goblet of Fire.txt" | wc -w
echo "Hermione:"
grep -io Hermione "Harry Potter and the Goblet of Fire.txt" | wc -w
echo "Dumbledore:"
grep -io Dumbledore "Harry Potter and the Goblet of Fire.txt" | wc -w
echo "Printing lines from 100 through 200 in the book:"
head -200 "Harry Potter and the Goblet of Fire.txt" | tail -100
echo "Counting number of unique words:"
tr -sc 'A-Za-z' '\n' < "Harry Potter and the Goblet of Fire.txt" | sort | uniq -c |wc -l

echo "Process ids (pid) and parent process ids(ppid) of Thunderbird:"
ps -ef | grep thunderbird
sleep 5
echo "Stopping the browser application:"
killall thunderbird
sleep 5
echo "Top 3 processes by CPU Usage:"
ps -eo pid,ppid,cmd,%cpu,%mem --sort=-%cpu | head -4
echo "Top 3 processes by Memory Usage:"
ps -eo pid,ppid,cmd,%cpu,%mem --sort=-%mem | head -4
echo "Starting a Python HTTP server on port 8000:"
sudo python3 -m http.server 8000
echo "Killing  Python HTTP server:"
sudo killall phython3
echo "Opening another tab:"
gnome -terminal --tab
echo "Killing the previous Process:"
sudo killall python3
echo "Display all active connections and the corresponding TCP / UDP ports:"
netstat -ltu 
sleep 3
echo "Pid of the process that is listening on port 5432:"sudo netstat -nlp |grep 5432
sudo apt install vim nginx htop
echo "Uninstalling nginx:"
sudo apt remove nginx
echo "Local IP address:"
hostname -I
echo "IP address of google.com:"
host www.google.com 
echo "checking Internet Connectivity:"
ping www.google.com
echo "finding paths of Code and Node :"
which code
which node



